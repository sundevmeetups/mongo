var MongoClient = require('mongodb').MongoClient;

//without cursor:

/* MongoClient.connect('mongodb://localhost:27017/course', function(err, db){
   if(err) throw err;

    var query = {assignment:'exam'};

    db.collection('grades').find(query).toArray(function(err, docs) {
        if(err) throw err;

        console.dir(docs);

        db.close();
    })

}); */

//with cursor:

MongoClient.connect('mongodb://localhost:27017/course', function(err, db){
   if(err) throw err;

    var query = {title: {'$regex' : 'world'}};
    var projection = { 'title': 1, '_id': 0 };

    var cursor = db.collection('reddit').find({}, projection);
    cursor.sort({'title' : 1 });
    cursor.each(function(err, doc) {
        if(err) throw err;

        if(doc == null) {
            return db.close();
        }
        console.log(doc.title);
    });

});
