var fs = require('fs');

var data = [];
var assignmentArr = ['exam', 'hw1', 'hw2', 'hw3'];

for(var i=0; i<20; i++) {
	var obj = {
		name: 'student_' + i,
		assignment: assignmentArr[i%4],
		grade: Math.round(Math.random() * 100)
	}

	data.push(JSON.stringify(obj));
}

data.toString = function() {
	var str = '';
	for(var i = 0; i < this.length; i++) {
		str += this[i] + '\n';
	}
	return str;
}

fs.writeFile('students.json',data,function(err) {
	if(err) throw err;
	console.log('It\'s saved!');
});
